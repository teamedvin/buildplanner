//
//  AddMachineViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-04.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class AddMachineViewController: UIViewController {
    
   

    @IBOutlet weak var nameLabel: UITextField!
    
    @IBOutlet weak var markLabel: UITextField!
    
    @IBOutlet weak var serialLabel: UITextField!
    
    @IBOutlet weak var fabriqueLabel: UITextField!
    
    @IBOutlet weak var typeLabel: UITextField!
    
    @IBOutlet weak var commentLabel: UITextField!
    @IBOutlet weak var colorButton: UIButton!
    var colorString: String = "#00822C008F04"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Lägg till maskin"
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func saveButton(sender: AnyObject) {
        
        if(nameLabel.text == "" || markLabel.text == "" || serialLabel.text == "" || fabriqueLabel.text == "" || typeLabel.text == ""){
            
            let alertController = UIAlertController(title: "Allt är inte ifyllt!", message:
                "Du måste fylla i alla fält för att kunna spara din maskin.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        
        } else {
            let authData = CURRENT_USER.key
            
            let machineKey = MACHINES_REF.childByAutoId()
            
            
            
            let newMachine = ["name": nameLabel.text,"mark":markLabel.text,"fabrique": fabriqueLabel.text,"type":typeLabel.text, "comment":commentLabel.text,"user": authData,"color": getRandomColorString(), "serial": serialLabel.text, "project": ProjectVariables.project, "moment" : "", "key" : machineKey.key, "start" : "", "end" : "","used" : "Används ej"]
            
            machineKey.setValue(newMachine)
            
            self.dismissViewControllerAnimated(true, completion: nil)
        
        }
      
        
    }
    
   
    @IBAction func cancelButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
  

    
}
