//
//  AddMomentViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-10.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class AddMomentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate  {
    
    @IBOutlet weak var numberOfWorkdaysLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var projectMachineTable: UITableView!
    @IBOutlet weak var momentMachineTable: UITableView!
    
    @IBOutlet weak var endDatePicker: UIDatePicker!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    var projectMachineArray = [NSDictionary]()
    var momentMachineArray = [NSDictionary]()
    let momentKey = MOMENTS_REF.childByAutoId()
    var momentOldKey : String = ""
    let calendar: NSCalendar = NSCalendar.currentCalendar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
      
        
        if momentOldKey != ""{
            loadDataFromFirebase(momentOldKey)
            loadOldMomentMachineFromFirebase()
           
        }
        
        loadDataFromFirebase()
        endDatePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        startDatePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        
        endDatePicker.setValue(false, forKey: "highlightsToday")
        startDatePicker.setValue(false, forKey: "highlightsToday")
        
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == projectMachineTable {
            return projectMachineArray.count
        } else {
            return momentMachineArray.count
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        if tableView == projectMachineTable {
            
            let cell:ProjectMachineTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1")! as! ProjectMachineTableViewCell
            if(projectMachineArray.count > 0){
                let dic = projectMachineArray[indexPath.row]
                cell.nameLabel.text = dic["name"] as? String
                cell.markLabel.text = dic["mark"] as? String
                cell.fabriqueLabel.text = dic["fabrique"] as? String
                cell.colorView.backgroundColor = colorWithHexString((dic["color"] as? String)!)
                let backgroundView = UIView()
                backgroundView.backgroundColor = UIColor(hue: 5/360, saturation: 100/100, brightness: 98/100, alpha: 0.5)
                cell.selectedBackgroundView = backgroundView
                
            }
            return cell
            
        } else {
            let cell:MomentMachineTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell2")! as! MomentMachineTableViewCell
            if(momentMachineArray.count > 0){
                let dic = momentMachineArray[indexPath.row]
                cell.nameLabel.text = dic["name"] as? String
                cell.markLabel.text = dic["mark"] as? String
                cell.fabriqueLabel.text = dic["fabrique"] as? String
                cell.colorView.backgroundColor = colorWithHexString((dic["color"] as? String!)!)
                let backgroundView = UIView()
                backgroundView.backgroundColor = UIColor(hue: 5/360, saturation: 100/100, brightness: 98/100, alpha: 0.2)
                cell.selectedBackgroundView = backgroundView
            }
            
            return cell
        }
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == projectMachineTable {
            if(projectMachineArray.count > 0){
                let dic = projectMachineArray[indexPath.row]
                momentMachineArray.append(dic)
                projectMachineArray.removeAtIndex(indexPath.row)
                projectMachineTable.reloadData()
                momentMachineTable.reloadData()
                
                
                for item in projectMachineArray {
                    let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                    let moment = ["moment": ""]
                    
                    
                    machineRef.updateChildValues(moment)
                }
                
                if(momentOldKey == ""){
                    for item in momentMachineArray {
                        let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                        
                        let moment = ["moment": momentKey.key]
                        
                        machineRef.updateChildValues(moment)
                    }
                } else {
                
                    for item in momentMachineArray {
                        let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                        
                        let moment = ["moment": momentOldKey]
                        
                        machineRef.updateChildValues(moment)
                    }

                }
                
            }
            
        } else {
            if(momentMachineArray.count > 0){
                let dic = momentMachineArray[indexPath.row]
                projectMachineArray.append(dic)
                momentMachineArray.removeAtIndex(indexPath.row)
                projectMachineTable.reloadData()
                momentMachineTable.reloadData()
                
                
                for item in projectMachineArray {
                    let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                    let moment = ["moment": ""]
                    let momentStart = ["start" : ""]
                    let momentEnd = ["end" : ""]
                    let used = ["used" : "Används ej"]
                    
                    machineRef.updateChildValues(momentStart)
                    machineRef.updateChildValues(momentEnd)
                    machineRef.updateChildValues(moment)
                    machineRef.updateChildValues(used)
                }
                
                if(momentOldKey == ""){
                    for item in momentMachineArray {
                        let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                        let moment = ["moment": momentKey.key]
                        let start = ["start" : dateAsString(startDatePicker.date)]
                        let end = ["end" : dateAsString(endDatePicker.date)]
                        
                        machineRef.updateChildValues(start)
                        machineRef.updateChildValues(end)
                        machineRef.updateChildValues(moment)
                    }
                } else {
                    for item in momentMachineArray {
                        let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                        let moment = ["moment": momentOldKey]
                        let start = ["start" : dateAsString(startDatePicker.date)]
                        let end = ["end" : dateAsString(endDatePicker.date)]
                        
                        machineRef.updateChildValues(start)
                        machineRef.updateChildValues(end)
                        machineRef.updateChildValues(moment)
                    }

                
                }
                
            }
            
            
        }
        
    }
    
    func loadDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        MACHINES_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                dict.setValue(child.key, forKey: "key")
                
                if (dict["project"] as? String == ProjectVariables.project && dict["moment"] as? String == "") {
                    
                    tempItems.append(dict)
                }
                
                
            }
            
            self.projectMachineArray = tempItems
            self.projectMachineArray = self.sortByName(self.projectMachineArray)
            self.projectMachineTable.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    func sortByName(list: [NSDictionary]) -> [NSDictionary]{
        
        
        let sorted = list.sort{
            guard let name = $0["type"] as? String,
                let name1 = $1["type"] as? String
                else {
                    print("something went wrong sorting")
                    return false
            }
            
            return name < name1
        }
        
        return sorted;
    }

    
    
    
    @IBAction func saveButton(sender: AnyObject) {
        
       
        
        
        let startDate = dateAsString(startDatePicker.date)
        
        let endDate = dateAsString(endDatePicker.date)
        let name = nameTextField.text as String!
        if(momentOldKey == ""){
            
             let color = getRandomColorString()
            let newMoment = ["name": name, "key": momentKey.key, "project" : ProjectVariables.project, "start": startDate, "end" : endDate, "color" : color]
            
            momentKey.setValue(newMoment)
        
        } else {
        
            let momentRef = MOMENTS_REF.childByAppendingPath(momentOldKey)
            let momentName = ["name" : name]
            let start = ["start" : startDate]
            let end = ["end" : endDate]
            
            
            momentRef.updateChildValues(start)
            momentRef.updateChildValues(end)
            momentRef.updateChildValues(momentName)

        
        }
        
       
            for item in momentMachineArray {
                let machineRef = MACHINES_REF.childByAppendingPath(item["key"] as? String)
                var moment = ["moment": momentKey.key]
                if(momentOldKey == ""){
                moment = ["moment": momentKey.key]
                } else {
                moment = ["moment": momentOldKey]
                
                }
                let start = ["start" : startDate]
                let end = ["end" : endDate]
                let used = ["used" : "Används"]
                
                machineRef.updateChildValues(start)
                machineRef.updateChildValues(end)
                machineRef.updateChildValues(moment)
                machineRef.updateChildValues(used)
            }

        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func startDatePicker(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        print("startDate")
        limitDatePicker()
       changeDaysLabel()
        
        
    }
    @IBAction func endDatePicker(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        limitDatePicker()
        changeDaysLabel()

        
        
    }
    
    func dateAsString(date: NSDate) -> String{
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let stringDate = dateFormatter.stringFromDate(date)
       
        return stringDate
        
    }
    @IBAction func cancelButton(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loadDataFromFirebase(key : String) {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        let ref = Firebase(url: "\(MOMENTS_REF)/\(key)")
        // Attach a closure to read the data at our posts reference
        ref.observeEventType(.Value, withBlock: { snapshot in
            
            let dic = snapshot.value as! NSDictionary
            self.nameTextField.text = dic["name"] as? String
            let firstDate = stringToDate((dic["start"] as? String)!)
            self.startDatePicker.date = firstDate
            let endDate = stringToDate((dic["end"] as? String)!)
            self.endDatePicker.date = endDate
            }, withCancelBlock: { error in
                print(error.description)
        })
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        
    }
    
    func loadOldMomentMachineFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        MACHINES_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                dict.setValue(child.key, forKey: "key")
                
                if (dict["project"] as? String == ProjectVariables.project && dict["moment"] as? String == self.momentOldKey) {
                    
                    tempItems.append(dict)
                }
                
                
            }
            
            self.momentMachineArray = tempItems
            self.momentMachineArray = self.sortByName(self.momentMachineArray)
            self.momentMachineTable.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.limitDatePicker()
            self.changeDaysLabel()
            
        })
    }

    
    func changeDaysLabel(){
        let days : Int = weekdaysBetween(calendar.startOfDayForDate(startDatePicker.date), secondDate: calendar.startOfDayForDate(endDatePicker.date))
        numberOfWorkdaysLabel.text = "Antal arbetsdagar: \(days)"
    }
    
    func limitDatePicker() {
    

        self.startDatePicker.maximumDate = self.endDatePicker.date
        self.endDatePicker.minimumDate = self.startDatePicker.date
    
    }
}




