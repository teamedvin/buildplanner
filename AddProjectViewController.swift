//
//  AddProjectViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-06.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class AddProjectViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var nameLabel: UITextField!
    
    @IBOutlet weak var projectNumberTextField: UITextField!
    @IBOutlet weak var projectBossTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Skapa nytt projekt"
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        descriptionTextView.delegate = self
        endDatePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        startDatePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        
        endDatePicker.setValue(false, forKey: "highlightsToday")
        startDatePicker.setValue(false, forKey: "highlightsToday")
        
        saveButton.titleLabel?.textColor = UIColor.whiteColor()
        
     
        
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if(descriptionTextView.text == "Beskrivning av projektet"){
            descriptionTextView.textColor = UIColor.blackColor()
            descriptionTextView.text = ""
        }
       
        self.view.frame.origin.y -= 150
    }
    
    
    func textViewDidEndEditing(textView: UITextView) {
        self.view.frame.origin.y += 150
    }
    
    
    func dismissKeyboard() {
      
        view.endEditing(true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveButton(sender: UIButton) {
        
        if(nameLabel.text == ""){
            let alertController = UIAlertController(title: "Saknar namn!", message:
                "Ditt projekt måste ha ett namn för att kunna sparas.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        
        } else {
        let authData = CURRENT_USER.key
        
        let startDate = dateAsString(startDatePicker.date)
        
        let endDate = dateAsString(endDatePicker.date)
        
        let projectKey = PROJECTS_REF.childByAutoId()
        
        let newProject = ["name": nameLabel.text, "user": authData, "startDate": startDate, "endDate": endDate, "projectRunner": USER_EMAIL, "description": descriptionTextView.text, "boss" : projectBossTextField.text, "number" : projectNumberTextField.text]
        
        projectKey.setValue(newProject)
        
        self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }

    @IBAction func startDatePickerAction(sender: AnyObject) {
       
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let startDate = dateFormatter.stringFromDate(startDatePicker.date)
            print("start =  \(startDate)")
            
        
    }
    
    @IBAction func endDatePickerAction(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let stopDate = dateFormatter.stringFromDate(endDatePicker.date)
        print("stop =  \(stopDate)")
    }

    
    func dateAsString(date: NSDate) -> String{
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let stringDate = dateFormatter.stringFromDate(date)
        print("Date =  \(date)")
        return stringDate
        
    }
  
    @IBAction func cancelButton(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
   

}
