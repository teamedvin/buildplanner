//
//  BaseService.swift
//
//  Created by Ebba on 2016-06-23.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import Foundation
import Firebase

let BASE_URL = "https://buildplanner.firebaseio.com"
let FIREBASE_REF = Firebase(url: BASE_URL)
let MACHINES_REF =  Firebase(url: BASE_URL + "/machines")
let PROJECTS_REF = Firebase(url: BASE_URL + "/projects")
let MOMENTS_REF = Firebase(url: BASE_URL + "/moments")

let USER_EMAIL = CURRENT_USER.authData.providerData["email"] as! String


var CURRENT_USER: Firebase
{
    let userId = NSUserDefaults.standardUserDefaults().valueForKey("uid") as! String
    let currentUser = Firebase(url:"\(FIREBASE_REF)").childByAppendingPath("users").childByAppendingPath(userId)
    return currentUser
}




struct ProjectVariables {
    static var project = "NoProject"
    
}