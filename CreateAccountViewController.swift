//
//  CreateAccountViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-06-23.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController {
    
    
    @IBOutlet weak var userNameLabel: UITextField!
    
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var repeatPasswordLabel: UITextField!
    
    override func viewDidLoad() {
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    @IBAction func createAccountButton(sender: AnyObject) {
        
        let email = userNameLabel.text
        let password = passwordLabel.text
        let repeatPassword = repeatPasswordLabel.text
        
        if password == repeatPassword{
            
            
            if email != "" && password != ""
            {
                FIREBASE_REF.createUser(email, password: password, withCompletionBlock: { (error) ->Void in
                    
                    if error == nil
                    {
                        FIREBASE_REF.authUser(email, password: password, withCompletionBlock: { (error, authData) -> Void in
                            if error == nil {
                                
                                NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: "uid")
                                print("Account created =) ");
                                
                                
                            }
                            else
                                
                            {
                                print(error)
                            }
                        })
                    }
                })
                
            } else {
                
                print("Fel!");
            }
            
            
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            
            let alertController = UIAlertController(title: "Ajdå!", message:
                "Du skrev inte samma lösenord båda gångerna. Gör om, gör rätt.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
    }
    @IBAction func cancelButton(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
