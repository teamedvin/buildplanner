//
//  DateService.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-10.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import Foundation

func daysLeftToString(toDateTime:String) -> String {
    
    let date = stringToDate(toDateTime)
    let days = daysBetweenThisDate(getTodaysDate(), andThisDate: date)
    return daysLeftAsString(days!)
}

func weekdaysBetween(firstDate: NSDate, secondDate: NSDate) -> Int{

    var date = firstDate
    var weekdays = 0
    if(firstDate == secondDate){
        return 1
    
    } else {
    
        
        while (date != secondDate) {
            
            if(dayOfWeek(date) == 7 || dayOfWeek(date) == 6){
                
                
            } else {
                weekdays += 1
                
            }
            
            let tomorrow = NSCalendar.currentCalendar().dateByAddingUnit(
                .Day,
                value: 1,
                toDate: date,
                options: NSCalendarOptions(rawValue: 0))
            date = tomorrow!
            
        }
        return weekdays
    
    }
 
}

func dayOfWeek(date: NSDate) -> Int? {
    if
        let cal: NSCalendar = NSCalendar.currentCalendar(),
        let comp: NSDateComponents = cal.components(.Weekday, fromDate: date) {
        return comp.weekday
    } else {
        return nil
    }
}

func daysBetweenThisDate(fromDateTime:NSDate, andThisDate toDateTime:NSDate)->Int?{
    
    var fromDate:NSDate? = nil
    var toDate:NSDate? = nil
    
    let calendar = NSCalendar.currentCalendar()
    
    calendar.rangeOfUnit(NSCalendarUnit.Day, startDate: &fromDate, interval: nil, forDate: fromDateTime)
    
    calendar.rangeOfUnit(NSCalendarUnit.Day, startDate: &toDate, interval: nil, forDate: toDateTime)
    
    if let from = fromDate {
        
        if let to = toDate {
            
            let difference = calendar.components(NSCalendarUnit.Day, fromDate: from, toDate: to, options: NSCalendarOptions())
            
            return difference.day
        }
    }
    
    return nil
}

func daysLeftFromToday(dateString: String) -> Int {
    let date = stringToDate(dateString)
    let days = daysBetweenThisDate(getTodaysDate(), andThisDate: date)
    return days!
    
}

func daysLeftFrom(firstDateString: String, toSecondDateString: String) -> Int {
    let date1 = stringToDate(firstDateString)
    let date2 = stringToDate(toSecondDateString)
    
    let days = daysBetweenThisDate(date1, andThisDate: date2)
    return days!
    
}

func stringToDate(dateString: String) -> NSDate {
    
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    let dateStart : NSDate = dateFormatter.dateFromString(dateString)!
    
    return dateStart
    
}

func getTodaysDate () -> NSDate{
    
    let date = NSDate()
    
    
    return date
    
}

func daysLeftAsString(daysLeft : Int) -> String {
    
    if daysLeft < -1 {
        
        return "\(daysLeft * -1) dagar försenat."
    } else if daysLeft == -1{
        return "En dag försenat."
        
    } else if daysLeft == 1{
        return "En dag kvar."
    } else if daysLeft == 0 {
        return "Slutar idag!"
    }else {
        return "\(daysLeft) dagar kvar."
    }
    
}

func getDatesForTwoWeeks() -> [NSDictionary] {
    
    var dates = [NSDictionary]()
    var i = 0
    while dates.count < 14{
        let today = NSDate()
        let nextDay = NSCalendar.currentCalendar().dateByAddingUnit(
            .Day,
            value: i,
            toDate: today,
            options: NSCalendarOptions(rawValue: 0))
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: nextDay!)
        let weekDay = myComponents.weekday
        
        let newDay = ["date" : getStringFromDate(nextDay!), "weekDay" : weekDay]
        dates.append(newDay)
        i += 1
    }
    
    
    
    return dates
}





func getStringFromDate(date: NSDate) -> String {
    let dateformatter = NSDateFormatter()
    dateformatter.dateFormat = "dd/MM"
    
    let stringDate = dateformatter.stringFromDate(date)
    
    return stringDate
}


func getStringFromDateDBFormat(date: NSDate) -> String {
    let dateformatter = NSDateFormatter()
    dateformatter.dateFormat = "dd-MM-yyyy"
    
    let stringDate = dateformatter.stringFromDate(date)
    
    return stringDate
}

func addOneDay(date : String) -> String {
    
    let tomorrow = NSCalendar.currentCalendar().dateByAddingUnit(
        .Day,
        value: 1,
        toDate: stringToDate(date),
        options: NSCalendarOptions(rawValue: 0))
    
    return getStringFromDateDBFormat(tomorrow!)
}

func backOneDay(date : String) -> String {
    
    let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(
        .Day,
        value: -1,
        toDate: stringToDate(date),
        options: NSCalendarOptions(rawValue: 0))
    
    return getStringFromDateDBFormat(yesterday!)
}

