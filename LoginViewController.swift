//
//  LoginViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-06-23.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UITextField!
    
    @IBOutlet weak var passwordLabel: UITextField!
    
    override func viewDidLoad() {
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        navigationController!.navigationBar.barTintColor = UIColor.blackColor()
        UIBarButtonItem.appearance().tintColor = UIColor.whiteColor()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.passwordLabel.text = ""
        if NSUserDefaults.standardUserDefaults().valueForKey("uid") != nil && CURRENT_USER.authData != nil
        {
            
        }
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @IBAction func logInButton(sender: AnyObject) {
        
        
        let email = emailLabel.text
        let password = passwordLabel.text
        
        if email != "" && password != ""
        {
            FIREBASE_REF.authUser(email, password: password, withCompletionBlock: {(error, authData)-> Void in
                if error == nil {
                    
                    NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: "uid")
                    
                    print("Loged in =)");
                    self.performSegueWithIdentifier("loginSegue", sender: self)
                    
                    
                    
                } else {
                    let alertController = UIAlertController(title: "Ajdå!", message:
                        "Ditt användarnamn passar inte ihop med ditt lösenord.", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    
                }
            })
        }
    }
    @IBAction func createAccountButton(sender: AnyObject) {
        
    }
    
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){
        
    }
    @IBAction func newPassword(sender: UIButton) {
       
        FIREBASE_REF.resetPasswordForUser(emailLabel.text, withCompletionBlock: { error in
            if error != nil {
                let alertController = UIAlertController(title: "Något blev fel!", message:
                    "Kanske har du inte fyllt i en giltig mailadress?", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                let alertController = UIAlertController(title: "Nytt lösenord skickat!", message:
                    "Vi rekommenderar att du ändrar ditt lösenord senare till något du kommer ihåg...", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                

            }
        })
        
        
    }
}
