//
//  MachineDetailViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-06.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class MachineDetailViewController: UIViewController {

    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var fabriqueLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var serialLabel: UILabel!
    @IBOutlet weak var markLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    var dic : NSDictionary = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.title = self.dic["name"] as? String
        
       
        commentLabel.text = self.dic["comment"] as? String
        markLabel.text = self.dic["mark"] as? String
        fabriqueLabel.text = self.dic["fabrique"] as? String
        serialLabel.text = self.dic["serial"] as? String
        typeLabel.text = self.dic["type"] as? String
        colorView.backgroundColor = colorWithHexString((self.dic["color"] as? String)!)
       
        commentLabel.backgroundColor = dic["color"] as? UIColor
        
        // Do any additional setup after loading the view.
        
        // Get a reference to our posts
               // Attach a closure to read the data at our posts reference
        
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Creates a UIColor from a Hex string.
    func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substringFromIndex(1)
        }
        
        
        
        let rString = (cString as NSString).substringToIndex(2)
        let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
        let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    

    

}
