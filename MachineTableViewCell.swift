//
//  MachineTableViewCell.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-04.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class MachineTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fabriqueLabel: UILabel!

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var markLabel: UILabel!
    
    @IBOutlet weak var usedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
