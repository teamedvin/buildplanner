//
//  MachineTableViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-04.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class MachineTableViewController: UITableViewController {
    
    
    
    var machines: [NSDictionary]  = []
    var machinesSorted: [NSDictionary] = []
    
    var names: Array = [String]()
    var sortedNames: Array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        navigationItem.title = "Alla Maskiner"
        tableView.backgroundView = UIImageView(image: UIImage(named: "bygge2"))
        
        self.loadDataFromFirebase()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    //override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    //return 1
    //}
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.machinesSorted.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell :MachineTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! MachineTableViewCell
        
        
        cell.nameLabel.text = machinesSorted[indexPath.row]["type"] as? String
        cell.markLabel.text = machinesSorted[indexPath.row]["mark"] as? String
        cell.fabriqueLabel.text = machinesSorted[indexPath.row]["fabrique"] as? String
        cell.colorView.backgroundColor = colorWithHexString((machinesSorted[indexPath.row]["color"] as? String)!)
        let usedText = machinesSorted[indexPath.row]["used"] as? String
        cell.usedLabel.text = usedText
        if(usedText == "Används ej"){
            cell.usedLabel.textColor = UIColor.redColor()
        } else {
            cell.usedLabel.textColor = UIColor.greenColor()
        }
        
        
        cell.backgroundColor = UIColor(hue: 25/360, saturation: 0/100, brightness: 6/100, alpha: 0.3)
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(hue: 5/360, saturation: 100/100, brightness: 98/100, alpha: 0.2)
        cell.selectedBackgroundView = backgroundView
        
        
        return cell
    }
    
    @IBAction func addMachine(sender: AnyObject) {
        
        
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            
            let dict = machinesSorted[indexPath.row]
            let key = dict["key"] as! String
            
            // delete data from firebase
            
            let firebaseRow = MACHINES_REF.childByAppendingPath(key)
            
            firebaseRow.removeValue()
            self.tableView.reloadData()
            
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let indexPath = self.tableView.indexPathForSelectedRow
        
        
        if (segue.identifier == "detailSegue"){
            let dic = machinesSorted[indexPath!.row]
            let destinationVC = segue.destinationViewController as! MachineDetailViewController
            
            destinationVC.dic = dic as NSDictionary
            
        }
        
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    func sortByName(list: [NSDictionary]) -> [NSDictionary]{
        
        
        let sorted = list.sort{
            guard let name = $0["type"] as? String,
                let name1 = $1["type"] as? String
                else {
                    print("something went wrong sorting")
                    return false
            }
            
            return name < name1
        }
        
        return sorted;
    }
    
    func loadDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        MACHINES_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                dict.setValue(child.key, forKey: "key")
                if(dict["project"] as? String == ProjectVariables.project){
                    tempItems.append(dict)
                }
                
                
            }
            
            self.machines = tempItems
            self.machinesSorted = self.sortByName(self.machines)
            self.tableView.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    
    
}
