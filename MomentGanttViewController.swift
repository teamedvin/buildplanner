//
//  MomentGanttViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-12.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class MomentGanttViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    
  
    var projectMoments = [NSDictionary]()
    
    var newGanttView: MomentsTwoWeeksGantView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MomentGanttViewController.instanciateVariables),name:"load", object: nil)
        
        
        
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        self.scrollView.scrollEnabled = true
        
        instanciateVariables()
        
    }
    
    func instanciateVariables(){
        scrollView.frame = CGRectMake(view.frame.maxX, view.frame.maxY, view.frame.width, view.frame.height);
        if((newGanttView) != nil){
            newGanttView.removeFromSuperview()
            
        }
        newGanttView = MomentsTwoWeeksGantView(frame: CGRectMake(5, 0, scrollView.frame.width-5, scrollView.frame.height))
        newGanttView.backgroundColor = UIColor.clearColor()
        scrollView.addSubview(newGanttView)
        
        scrollView.contentOffset = CGPoint(x: 10, y: 20)        
        loadDataFromFirebase()
        newGanttView.dates = getDatesForTwoWeeks()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.newGanttView
    }
    
    func loadDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        
        MOMENTS_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                
                if(dict["project"] as? String == ProjectVariables.project){
                    
                    tempItems.append(dict)
               
                }
                
                
            }
                        self.newGanttView.moments = tempItems
            
            self.projectMoments = tempItems
            self.loadMachineDataFromFirebase()
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    func loadMachineDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        
        MACHINES_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                
                if(dict["project"] as? String == ProjectVariables.project){
                    
                    
                    let timeToStart = daysLeftFromToday((dict["start"] as? String)!)
                    let duration = daysLeftFrom((dict["start"] as? String)!, toSecondDateString: (dict["end"] as? String)!)
                    dict.setValue(timeToStart, forKey: "start")
                    dict.setValue(duration, forKey: "stop")
                    tempItems.append(dict)
                    
                }
                
            }
            
            self.newGanttView.machines = tempItems
            
            self.newGanttView.setNeedsDisplay()
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    override func viewWillDisappear(animated: Bool)
    {
       print("BORTA")
    }
}