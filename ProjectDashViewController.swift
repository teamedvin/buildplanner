//
//  ProjectDashViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-10.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class ProjectDashViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var dic : NSDictionary = [:]
    
    @IBOutlet weak var momentsTableView: UITableView!
    @IBOutlet weak var descriptiontextView: UITextView!
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var totalDaysLabel: UILabel!
    @IBOutlet weak var projectRunnerLabel: UILabel!
    
    var momentsArray = [NSDictionary]()
    var momentsSorted = [NSDictionary]()
    var oldKey : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = dic["name"] as? String
        projectRunnerLabel.text = "Platschef: \(dic["boss"] as! String)"
        
        let dateStart : NSDate = stringToDate(dic["startDate"] as! String)
        let dateStop : NSDate = stringToDate(dic["endDate"] as! String)
        let totalDays : Int = daysBetweenThisDate(dateStart, andThisDate: dateStop)!
        let daysLeft : Int = daysBetweenThisDate(getTodaysDate(), andThisDate: dateStop)!
        
        daysLeftLabel.text = daysLeftAsString(daysLeft)
        
        totalDaysLabel.text = "\(totalDays) beräknade dagar för projektet."
        
        descriptiontextView.text = dic["description"] as? String
        
        navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        
        loadDataFromFirebase()
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        oldKey = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        momentsTableView.reloadData()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return momentsSorted.count // your number of cell here
    }
    
   
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell :MomentsTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! MomentsTableViewCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.redColor()
        cell.selectedBackgroundView = backgroundView
        cell.momentNameLabel.text = momentsSorted[indexPath.row]["name"] as? String
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        oldKey = (momentsSorted[indexPath.row]["key"] as? String)!
        performSegueWithIdentifier("addMoment", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if (segue.identifier == "addMoment"){
            
            let destinationVC = segue.destinationViewController as! AddMomentViewController
            
            
            destinationVC.momentOldKey = oldKey
            
            
            
        }
       
    }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            
            let dict = momentsSorted[indexPath.row]
            let key = dict["key"] as! String
            
            // delete data from firebase
            
            let firebaseRow = MOMENTS_REF.childByAppendingPath(key)
            
            firebaseRow.removeValue()
            self.momentsTableView.reloadData()
        }
    }
    
    func loadDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        MOMENTS_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                dict.setValue(child.key, forKey: "key")
                if(dict["project"] as? String == ProjectVariables.project){
                    tempItems.append(dict)
                }
                
                
            }
            
            self.momentsArray = tempItems
            self.momentsSorted = self.sortByName(self.momentsArray)
            self.momentsTableView.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    
    func sortByName(list: [NSDictionary]) -> [NSDictionary]{
        
        
        let sorted = list.sort{
            guard let name = $0["name"] as? String,
                let name1 = $1["name"] as? String
                else {
                    print("something went wrong sorting")
                    return false
            }
            
            return name < name1
        }
        
        return sorted;
    }
    @IBAction func logOutButton(sender: UIBarButtonItem) {
        CURRENT_USER.unauth()
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "uid")
        
        self.performSegueWithIdentifier("unwind", sender: self)
    }
}


