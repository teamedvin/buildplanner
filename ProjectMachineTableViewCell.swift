//
//  ProjectMachineTableViewCell.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-10.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class ProjectMachineTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fabriqueLabel: UILabel!
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var markLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
