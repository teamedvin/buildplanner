//
//  ProjectTableViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-06.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import Firebase
import UIKit

class ProjectTableViewController: UITableViewController {
    
    var projects: [NSDictionary]  = []
    var projectsSorted: [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataFromFirebase()
        
        title = "Mina projekt"
        
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "bygge2"))
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    /*
     override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 1
     }
     */
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return projectsSorted.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell :ProjectTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! ProjectTableViewCell
        // Configure the cell...
        
        
        cell.nameLabel.text = projectsSorted[indexPath.row]["name"] as? String
               cell.daysLeftLabel.text = daysLeftToString((projectsSorted[indexPath.row]["endDate"] as? String)!)
        cell.backgroundColor = UIColor(hue: 25/360, saturation: 0/100, brightness: 6/100, alpha: 0.3)
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(hue: 5/360, saturation: 100/100, brightness: 98/100, alpha: 0.2)
        cell.selectedBackgroundView = backgroundView
        return cell
    }
  
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            
            let dict = projectsSorted[indexPath.row]
            let key = dict["key"] as! String
            
            // delete data from firebase
            
            let firebaseRow = PROJECTS_REF.childByAppendingPath(key)
            
            firebaseRow.removeValue()
            self.tableView.reloadData()
        }
    }
   
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let indexPath = self.tableView.indexPathForSelectedRow
        
        
        if (segue.identifier == "projectSegue"){
            let dic = projectsSorted[indexPath!.row]
            
            let destinationVC = segue.destinationViewController as! ProjectDashViewController
            ProjectVariables.project = (dic["key"] as? String)!
            
            destinationVC.dic = dic
            
            
            
        }
        
    }
    
    
    
    func loadDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        PROJECTS_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                dict.setValue(child.key, forKey: "key")
                
                if (dict["user"] as? String == CURRENT_USER.key) {
                    tempItems.append(dict)
                }
                
                
            }
            
            self.projects = tempItems
            self.projectsSorted = self.sortByName(self.projects)
            self.tableView.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    func sortByName(list: [NSDictionary]) -> [NSDictionary]{
        
        
        let sorted = list.sort{
            guard let name = $0["name"] as? String,
                let name1 = $1["name"] as? String
                else {
                    print("something went wrong sorting")
                    return false
            }
            
            return name < name1
        }
        
        return sorted;
    }
    
    
    
    @IBAction func logOutButton(sender: UIBarButtonItem) {
        CURRENT_USER.unauth()
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "uid")
        
        self.performSegueWithIdentifier("unwind", sender: self)
        
    }
}
