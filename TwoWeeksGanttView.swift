//
//  TwoWeeksGanttView.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-10.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class TwoWeeksGanttView: UIView {
    
    var dates = [NSDictionary]()
    var machines = [NSDictionary]()
    
    var dayWidth: CGFloat = 0.0
    
    override func drawRect(rect: CGRect) {
        
        
        dayWidth = bounds.size.width/10
        let yContext = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(yContext, UIColor.whiteColor().CGColor)
        
        CGContextMoveToPoint(yContext, 0 , (bounds.size.height/8))
        CGContextAddLineToPoint(yContext, 0, bounds.size.height)
        
        CGContextStrokePath(yContext)
        
        let xContext = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(xContext, UIColor.whiteColor().CGColor)
        
        CGContextMoveToPoint(xContext, 0 , (bounds.size.height/8))
        CGContextAddLineToPoint(xContext, bounds.size.width, (bounds.size.height/8))
        
        CGContextStrokePath(xContext)

        
        drawLineLoop()
        
          var date:Double =  Double(bounds.size.height/7)
        
        for i in 0..<machines.count {
            
            let startMachine = (machines[i]["start"] as! Double) * Double(dayWidth)
            let stopMachine = (machines[i]["stop"] as! Double) * Double(dayWidth)
            let color =  colorWithHexString(machines[i]["color"] as! String).CGColor
            let name = machines[i]["name"] as! String
            
            
            
            drawTimeRect(CGFloat(startMachine), startY: CGFloat(date), width: CGFloat(stopMachine), color: color, name: name, index: i)
            
            date+=Double(bounds.size.height/22)
        }
        
        
    }
    
    func drawTimeRect(startX: CGFloat, startY: CGFloat, width: CGFloat, color: CGColor, name: String, index : Int){
        
        let button = UIButton();
        button.setTitle(name, forState: .Normal)
        
        button.frame = CGRect(x: startX - 10, y: startY, width: width,height: 28)// X, Y, width, height
        button.addTarget(self, action: #selector(TwoWeeksGanttView.buttonPressed(_:)), forControlEvents: .TouchUpInside)
         button.backgroundColor = UIColor(CGColor: color)
        button.tag = index
        self.addSubview(button)
   
        
        
    }

    func buttonPressed(sender: UIButton!) {
        let moment : String = machines[sender.tag]["moment"] as! String
        
        let color : String = machines[sender.tag]["color"] as! String
     
        let infoView = UIView(frame: CGRectMake(0, 0, bounds.size.width, 20))
        infoView.backgroundColor = colorWithHexString(color)
        
        self.addSubview(infoView)
        
        loadMomentNameFromFirebase(moment)
        
    }
    
    func drawLineDown(x : CGFloat, view: UIView, startX: CGFloat, date: String, weekday: Int){
        
        
        let label = UILabel(frame: CGRectMake(x-startX + 15, (bounds.size.height/11), 100, 30))
        
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.systemFontOfSize(20)
        label.textAlignment = NSTextAlignment.Left
        label.text = date
        
        view.addSubview(label)
        
        let lineContext = UIGraphicsGetCurrentContext()
        
        if weekday == 7 || weekday == 6 || weekday == 1 {
            CGContextSetStrokeColorWithColor(lineContext, UIColor.redColor().CGColor)
            CGContextSetLineWidth(lineContext, 4.0)
            
        } else {
            CGContextSetStrokeColorWithColor(lineContext, UIColor.whiteColor().CGColor)
            CGContextSetLineWidth(lineContext, 1.0)
        }
        
        
        CGContextMoveToPoint(lineContext, x , (bounds.size.height/10))
        CGContextAddLineToPoint(lineContext, x, bounds.size.height)
        
        CGContextStrokePath(lineContext)
        
        
    }
    func drawLineRight(y : CGFloat){
        
        let lineContext = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(lineContext, UIColor.whiteColor().CGColor)
        
        CGContextMoveToPoint(lineContext, 0 , y)
        CGContextAddLineToPoint(lineContext, bounds.size.width-130, y)
        
        CGContextStrokePath(lineContext)
    }
    
    
    
    func drawLineLoop(){
        var i = bounds.size.width/10 - 10
        var dateIndex: Int = 0
        let start = bounds.size.width/10
        
        while i < bounds.size.width {
            let weekday = (dates[dateIndex]["weekDay"] as? Int)!
            if weekday == 7 || weekday == 1 {
                print("no day! weekday")
            } else {
                drawLineDown(CGFloat(i), view: self, startX: start, date: (dates[dateIndex]["date"] as? String)!, weekday:weekday )
                
                i = i + bounds.size.width/10
                
            }
            dateIndex += 1
        }
        
        
        
    }
    
    
    func loadMomentNameFromFirebase(momentKey : String) {
    
        
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        MOMENTS_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                dict.setValue(child.key, forKey: "key")
                
                if (dict["key"] as? String == momentKey) {
                    
                    tempItems.append(dict)
                }
                
                
            }
            
            let label = UILabel(frame: CGRectMake((self.bounds.size.width/4), 1, (self.bounds.size.width/2), 20))
            label.backgroundColor = UIColor.clearColor()
            label.font = UIFont.systemFontOfSize(20)
            label.textAlignment = NSTextAlignment.Center
            label.text = tempItems[0]["name"] as? String
            
            self.addSubview(label)
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    
        }

}
