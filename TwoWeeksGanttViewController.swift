//
//  TwoWeeksGanttViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-10.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class TwoWeeksGanttViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var unusedButton: UIButton!
 
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var newGanttView: TwoWeeksGanttView!
    
    var unusedMachines = Int()
    var projectMachines = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().barTintColor = UIColor.blackColor()
        UITabBar.appearance().tintColor = UIColor.whiteColor()
     
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        self.scrollView.scrollEnabled = true
       
        
        instanciateVariables()
        
       
        
    }
    
    override func viewWillAppear(animated: Bool) {
       instanciateVariables()
    }
    
    
    
    
    func instanciateVariables(){
       scrollView.frame = CGRectMake(view.frame.maxX, view.frame.maxY, view.frame.width, view.frame.height);
        if((newGanttView) != nil){
            newGanttView.removeFromSuperview()
            
        }
        
        
        newGanttView = TwoWeeksGanttView(frame: CGRectMake(5, 0, scrollView.frame.width-5, scrollView.frame.height))
        
        
        loadDataFromFirebase()
        newGanttView.dates = getDatesForTwoWeeks()
        newGanttView.backgroundColor = UIColor.clearColor()
       scrollView.addSubview(newGanttView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.newGanttView
    }
    
    func loadDataFromFirebase() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        
        MACHINES_REF.observeEventType(.Value, withBlock: { snapshot in
            var tempItems = [NSDictionary]()
            
            for item in snapshot.children {
                
                let child = item as! FDataSnapshot
                
                let dict = child.value as! NSDictionary
                
                if(dict["project"] as? String == ProjectVariables.project && dict["moment"] as? String != ""){
                    
                    
                    let timeToStart = daysLeftFromToday((dict["start"] as? String)!)
                  
                    let duration : Int = weekdaysBetween(stringToDate((dict["start"] as? String)!), secondDate: stringToDate((dict["end"] as? String)!))
                    
                    dict.setValue(timeToStart, forKey: "start")
                    dict.setValue(duration, forKey: "stop")
                    tempItems.append(dict)
                    
                } else if dict["project"] as? String == ProjectVariables.project {
                    
                    self.unusedMachines += 1
                }
                
                
            }
            self.unusedButton.setTitle("\(String(self.unusedMachines)) maskiner används ej.", forState: .Normal)
            
            self.newGanttView.machines = tempItems
            
            self.projectMachines = tempItems
            self.newGanttView.setNeedsDisplay()
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            
        })
    }
    
    
}