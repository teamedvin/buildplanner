//
//  UserDetailViewController.swift
//  BuildPlanner
//
//  Created by Ebba on 2016-07-11.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import Firebase

class UserDetailViewController: UIViewController {

    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var repeatPasswordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        userEmailLabel.text = (CURRENT_USER.authData.providerData["email"] as! String)
        
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func savePassword(sender: UIButton) {
        
        if(newPasswordField.text == repeatPasswordField.text){
        
            
            FIREBASE_REF.changePasswordForUser(userEmailLabel.text, fromOld: oldPasswordField.text,
                                               toNew: newPasswordField.text, withCompletionBlock: { error in
                                                if error != nil {
                                                  
                                                    let alertController = UIAlertController(title: "Ajdå!", message:
                                                        "Något blev fel...", preferredStyle: UIAlertControllerStyle.Alert)
                                                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                                                    
                                                    self.presentViewController(alertController, animated: true, completion: nil)

                                                } else {
                                                    let alertController = UIAlertController(title: "Hurra!", message:
                                                        "Lösenordet är ändrat!", preferredStyle: UIAlertControllerStyle.Alert)
                                                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                                                    
                                                    self.presentViewController(alertController, animated: true, completion: nil)

                                                }
            })
        } else {
            let alertController = UIAlertController(title: "Ajdå!", message:
                "Du måste skriva samma nya lösenord två gånger.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        
        }
        
    }
    @IBAction func cancelButton(sender: UIButton) {
          self.dismissViewControllerAnimated(true, completion: nil)
    }
}
